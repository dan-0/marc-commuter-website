---
title: "App"
date: 2018-06-10T20:04:44-05:00
draft: false
---

<div id="screenshots" class="carousel slide" data-interval="false" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#screenshots" data-slide-to="0" class="active"></li>
    <li data-target="#screenshots" data-slide-to="1"></li>
    <li data-target="#screenshots" data-slide-to="2"></li>
    <li data-target="#screenshots" data-slide-to="3"></li>
    <li data-target="#screenshots" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="/img/screenshot/Screenshot_20180611-143223.png" alt="/img/icon-full.svg">
      <div class="carousel-caption">
        Train statuses
      </div>
    </div>
    <div class="item">
      <img src="/img/screenshot/Screenshot_20180611-143228.png" alt="/img/icon-full.svg">
      <div class="carousel-caption">
        Train statuses
      </div>
    </div>
    <div class="item">
      <img src="/img/screenshot/Screenshot_20180611-153211.png" alt="/img/icon-full.svg">
      <div class="carousel-caption">
        Train statuses
      </div>
    </div>
    <div class="item">
      <img src="/img/screenshot/Screenshot_20180611-182818.png" alt="/img/icon-full.svg">
      <div class="carousel-caption">
        Alerts
      </div>
    </div>
    <div class="item">
      <img src="/img/screenshot/Screenshot_20180611-121632.png" alt="/img/icon-full.svg">
      <div class="carousel-caption">
        Schedules
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#screenshots" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#screenshots" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

Navigate the MARC with ease whether commuting, on a day trip to Harpers Ferry, or
coming home from BWI. MARC Commuter helps you get where you need to go by providing
you the best information available to track the train you need to be on.

<ul class="text-center list-unstyled list-inline">
    <li>
        <a href='https://play.google.com/store/apps/details?id=com.idleoffice.marctrain' target="_blank">
            <img class="storeLink" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png"/>
        </a>
    </li>
    <li>
        <a href="https://gitlab.com/dan-0/MarcTrainTracker" style="color:#fd7e14;">
            <img class="storeLink" alt="GitLab" src="/img/gitlab.svg"/>
        </a>
    </li>
</ul>
